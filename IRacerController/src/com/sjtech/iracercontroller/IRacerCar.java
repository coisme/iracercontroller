package com.sjtech.iracercontroller;

public class IRacerCar {
	// DIRECTION COMMAND SET
	public static final int CMD_STOP = 0x00;
	public static final int CMD_STRAIGHT_FORWARD = 0x10;
	public static final int CMD_STRAIGHT_BACKWARD = 0x20;
	public static final int CMD_LEFT_NO_DRIVE = 0x30;
	public static final int CMD_RIGHT_NO_DRIVE = 0x40;
	public static final int CMD_LEFT_FORWARD = 0x50;
	public static final int CMD_RIGHT_FORWARD = 0x60;
	public static final int CMD_LEFT_BACKWARD = 0x70;
	public static final int CMD_RIGHT_BACKWARD = 0x80;
	
	// SPEED COMMAND SET
	public static final int CMD_SPEED_STOP = 0x00;
	public static final int CMD_SPEED_LOWEST = 0x01;
	public static final int CMD_SPEED_HIGHEST = 0x0F;
	
	// Serial Communication Parameters
	public static final int BAUD_RATE = 9600;     // bit per second
	public static final int DATA_LENGTH = 8;      // bits
	public static final boolean PARITY = false;
	public static final int STOP_BIT = 1;     // bit
}
