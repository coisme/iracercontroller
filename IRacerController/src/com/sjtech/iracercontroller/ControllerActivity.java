package com.sjtech.iracercontroller;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import android.os.Bundle;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.content.res.Resources;
import android.util.Log;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

public class ControllerActivity extends Activity implements OnTouchListener{
    private static final String TAG = "iRacerController";
    private static final boolean D = true;
	private static final int REQUEST_ENABLE_BT = 1;
	private static final float DEFAULT_SENSITIVITY = 0.1f;

	private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
	
	// Variables for Bluetooth Connection
	private BluetoothAdapter mBluetoothAdapter;
	private BluetoothSocket mBluetoothSocket;
	private ConnectThread mConnectThread;
	private OutputStream mOutputStream;
	private List<String> mList;
	private ToggleButton toggleButton;

	private ImageView controlView;

	private boolean isConnected;
	
	// Initial touch position
	private float originX, originY;
	private float sensitivity;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_controller);

		// initialize the member variables
		this.sensitivity = ControllerActivity.DEFAULT_SENSITIVITY;
		this.isConnected = false;
		
		// setup controller view
		this.controlView = (ImageView)findViewById( R.id.imageViewController);
		this.controlView.setOnTouchListener( this);
		
		// setup toggle button
		this.toggleButton = (ToggleButton)findViewById( R.id.toggleButton1);
		
		// initialize List
		mList = new ArrayList<String>();
		
		// Check bluetooth support
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		if( mBluetoothAdapter == null){
			// Bluetooth is not supported.
			if( D) Log.e( TAG, "Bluetooth is not supported.");
            Toast.makeText(this, getString( R.string.bt_not_supported), Toast.LENGTH_LONG).show();
            return;
		}
		
		// Check bluetooth availability
		if (!mBluetoothAdapter.isEnabled()) {
		    Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
		    startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
		    // onActivityResult will be called when bluetooth is turned on by user.
		}
		else{
			setupBluetoothConnection();
		}		
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if( requestCode == REQUEST_ENABLE_BT){
			if( resultCode == Activity.RESULT_OK){
				// User enabled bluetooth connection.
				setupBluetoothConnection();
			}
			else{
				// User declined  bluetooth connection.
	            Toast.makeText(this, getString( R.string.bt_not_available), Toast.LENGTH_LONG).show();
			}
		}

		super.onActivityResult(requestCode, resultCode, data);
	}

	/* +++++++++++++++++++++++++++++
	 * Bluetooth Connection
	 * +++++++++++++++++++++++++++++
	 */
	private void setupBluetoothConnection(){
		if( D) Log.e( TAG, "+++ setupBluetoothConnection +++");
		Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
		// If there are paired devices
		if (pairedDevices.size() > 0) {
		    // Loop through paired devices
		    for (BluetoothDevice device : pairedDevices) {
		    	mList.add(device.getName() + "\n" + device.getAddress());
		    }
		}
		else{
			// There is no paired device.
			mList.add( this.getString( R.string.no_paired_device));
		}
		// Set the paired device names to the spinner
		ArrayAdapter<String> dataAdapter = 
				new ArrayAdapter<String>( this, android.R.layout.simple_spinner_dropdown_item, mList);
		Spinner spinner = (Spinner)findViewById( R.id.spinner_select_device);
		dataAdapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter( dataAdapter);
	}
	
    private class ConnectThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final BluetoothDevice mmDevice;
     
        public ConnectThread(BluetoothDevice device) {
            // Use a temporary object that is later assigned to mmSocket,
            // because mmSocket is final
            BluetoothSocket tmp = null;
            mmDevice = device;
     
            // Get a BluetoothSocket to connect with the given BluetoothDevice
            try {
                // MY_UUID is the app's UUID string, also used by the server code
                tmp = device.createInsecureRfcommSocketToServiceRecord(MY_UUID);
            } catch (IOException e) { }
            mmSocket = tmp;
        }
     
        public void run() {
			// Cancel discovery because it will slow down the connection
            // mBluetoothAdapter.cancelDiscovery();
            try {
                // Connect the device through the socket. This will block
                // until it succeeds or throws an exception
                mmSocket.connect();
            } catch (IOException connectException) {
                // Unable to connect; close the socket and get out
                try {
                    mmSocket.close();
                } catch (IOException closeException) { }
                if( D) Log.e( TAG, "Unable to connect.");
                return;
            }
            // Do work to manage the connection (in a separate thread)
            connected(mmSocket);
        }
     
        /** Will cancel an in-progress connection, and close the socket */
        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) { }
        }
    }
	
    private void connected( BluetoothSocket sock){
    	mBluetoothSocket = sock;
		// Bluetooth connection has established.
    	if( D) Log.e( TAG, getString( R.string.bt_established));
    	try {
			mOutputStream = mBluetoothSocket.getOutputStream();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	isConnected = true;
    }
	
	private void disconnect(){
		if( mConnectThread != null){
			mConnectThread.cancel();
			mConnectThread = null;
		}
		if( mBluetoothSocket != null){
			try {
				mBluetoothSocket.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			mBluetoothSocket = null;
		}
		if( D) Log.e( TAG, getString( R.string.bt_disconnect));
	}
	
	/*
	 * User Interface
	 * 
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.controller, menu);
		return true;
	}

	
	public void onToggleClicked( View view){
		if( view == this.toggleButton){
			boolean isConnect = ((ToggleButton)view).isChecked();
		
			if( isConnect){
				// Get the device MAC address, which is the last 17 chars in the View
				Spinner spinner = (Spinner)findViewById( R.id.spinner_select_device);
				String info = (String)spinner.getSelectedItem();
				String address = info.substring( info.length() - 17);
            
				BluetoothDevice device = mBluetoothAdapter.getRemoteDevice( address);

				if( mConnectThread != null){
					mConnectThread.cancel();
					mConnectThread = null;
				}

				Toast.makeText(this, getString( R.string.bt_connect) + address, Toast.LENGTH_LONG).show();

				if( D) Log.e( TAG, "Connecting to " + address);
			
				mConnectThread = new ConnectThread( device);
				mConnectThread.start();
			}
			else{
				disconnect();
			}
		}
	}
	
	@Override
	public boolean onTouch(View v, MotionEvent event) {		
		boolean isEventConsumed = false;
		if( v == this.controlView){
			int action = event.getAction();
			if( isConnected){
				// User touched the screen
				if( action == MotionEvent.ACTION_DOWN){
					originX = event.getX();
					originY = event.getY();
					
					isEventConsumed = true;
				}
				// User's finger is moving on the screen
				else if( action == MotionEvent.ACTION_MOVE){
					float dx = event.getX() - originX;
					float dy = event.getY() - originY;
					
					// calculate the car speed
					int length = (int)(Math.sqrt( dx*dx + dy*dy)*sensitivity);
					int speed = IRacerCar.CMD_SPEED_LOWEST + length;
					if( speed > IRacerCar.CMD_SPEED_HIGHEST) speed = IRacerCar.CMD_SPEED_HIGHEST;
					
					// get the car direction
					int dir = getDirection( dx, dy);
					
					// move the car
					try {
						this.mOutputStream.write( dir + speed);
					}
					catch (IOException e){
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					isEventConsumed = true;
				}
				// The finger is off from the screen
				else if( action == MotionEvent.ACTION_UP){
					// stop
					try {
						this.mOutputStream.write( IRacerCar.CMD_STOP);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					isEventConsumed = true;
				}
				else{
					// Nothing to do.
				}
			}
		}
		return isEventConsumed;
	}

	@Override
	protected void onStop() {
		disconnect();
		super.onStop();
	}
	
	private int getDirection( float dx, float dy){
		final double unitAngle = Math.PI/8.0;
		double angle = Math.atan2( -dy, dx);
		int ret = IRacerCar.CMD_STOP;

		if( -8*unitAngle < angle && angle <= -7*unitAngle){
			ret = IRacerCar.CMD_LEFT_NO_DRIVE;
		}
		else if( -7*unitAngle < angle && angle <= -5*unitAngle){
			ret = IRacerCar.CMD_LEFT_BACKWARD;
		}
		else if( -5*unitAngle < angle && angle <= -3*unitAngle){
			ret = IRacerCar.CMD_STRAIGHT_BACKWARD;
		}
		else if( -3*unitAngle < angle && angle <= -unitAngle){
			ret = IRacerCar.CMD_RIGHT_BACKWARD;
		}
		else if( -unitAngle < angle && angle <= unitAngle){
			ret = IRacerCar.CMD_RIGHT_NO_DRIVE;
		}
		else if( unitAngle < angle && angle <= 3*unitAngle){
			ret = IRacerCar.CMD_RIGHT_FORWARD;
		}
		else if( 3*unitAngle < angle && angle <= 5*unitAngle){
			ret = IRacerCar.CMD_STRAIGHT_FORWARD;
		}
		else if( 5*unitAngle < angle && angle <= 7*unitAngle){
			ret = IRacerCar.CMD_LEFT_FORWARD;
		}
		else if( 7*unitAngle < angle && angle <= 8*unitAngle){
			ret = IRacerCar.CMD_LEFT_NO_DRIVE;
		}
		else{
			// Nothing to do.
		}
		
		return ret;
	}
}
